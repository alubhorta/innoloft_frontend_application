import React from "react";
import { useSelector } from "react-redux";

import Content from "./components/layout/Content";
import Footer from "./components/layout/Footer";
import Header from "./components/layout/Header";
import Sidebar from "./components/layout/Sidebar";
import LogOutPage from "./components/pages/LogOutPage";
import { User } from "./models";

const AccessControl: React.FC = () => {
  const loggedIn = useSelector((state: { user: User }) => state.user.loggedIn);

  if (!loggedIn) {
    return <LogOutPage />;
  }

  return (
    <>
      <Header />
      <Sidebar />
      <Content />
      <Footer />
    </>
  );
};

export default AccessControl;
