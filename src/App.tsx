import React from "react";
import { Provider } from "react-redux";

import store from "./store";
import AccessControl from "./AccessControl";

import "./styles/index.scss";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <AccessControl />
      </div>
    </Provider>
  );
}

export default App;
