import { AccountInfo, UserInfo } from "../models";

export class MockApiClient {
  async updateAccountsInfo(accInfo: AccountInfo) {
    /* make fetch/Ajax here */
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(true);
      }, 500);
    });
  }
  
  async updateUnserInfo(userInfo: UserInfo) {
    /* make fetch/Ajax here */
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(true);
      }, 500);
    });
  }
}
