export type AccountInfo = {
  email: string;
  password: string;
};

export type UserInfo = {
  firstName: string;
  lastName: string;
  country: string;
  address: {
    street: string;
    houseNum: string;
    postCode: string;
  };
};

export type User = {
  userInfo: UserInfo,
  email: string;
  loggedIn: boolean
}
