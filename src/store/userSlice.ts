import { createSlice } from "@reduxjs/toolkit";
import { User } from "../models";

const mockUser: User = {
  email: "jon.is.a@berliner.io",
  loggedIn: true,
  userInfo: {
    firstName: "Jon",
    lastName: "Doe",
    country: "Germany",
    address: {
      street: "Bundesplatz",
      houseNum: "8",
      postCode: "10715",
    },
  },
};

const userSlice = createSlice({
  name: "user",
  initialState: mockUser,
  reducers: {
    login: (state) => {
      state.loggedIn = true;
      return state;
    },
    logout: (state) => {
      state.loggedIn = false;
      return state;
    },
    updateUserInfo: (state, action) => {
      state.userInfo = action.payload.userInfo;
      return state;
    },
    updateEmail: (state, action) => {
      state.email = action.payload.email;
      return state;
    },
  },
});

export const { login, logout, updateEmail, updateUserInfo } = userSlice.actions;

export default userSlice.reducer;

