import React from "react";

const LogOutPage: React.FC = () => {
  return (
    <>
      <div className="container text-center">
        <h1>Logged Out</h1>
        <p>
          You're logged out. Did you know that refreshing the page will log you
          in? {"🤷"}
        </p>
      </div>
    </>
  );
};

export default LogOutPage;
