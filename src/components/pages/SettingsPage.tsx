import React, { useState } from "react";
import AccountsTab from "../tabs/AccountsTab";
import UserInfoTab from "../tabs/UserInfoTab";

const SettingsPage: React.FC = () => {
  const [openedTabIdx, setOpenedTabIdx] = useState(0);

  const getCurrentTab = () =>
    openedTabIdx === 0 ? <AccountsTab /> : <UserInfoTab />;

  return (
    <>
      <div className="container">
        <h1 className="text-center">Settings</h1>
        <div className="tabs">
          <div className="tab-btns">
            <button
              onClick={() => setOpenedTabIdx(0)}
              className={openedTabIdx === 0 ? "active" : ""}
            >
              Accounts
            </button>
            <button
              onClick={() => setOpenedTabIdx(1)}
              className={openedTabIdx === 1 ? "active" : ""}
            >
              User Info
            </button>
          </div>

          <div className="tab-content">{getCurrentTab()}</div>
        </div>
      </div>
    </>
  );
};

export default SettingsPage;
