import React, { useState } from "react";

import { PasswordMeter } from "password-meter";
import { MockApiClient } from "../../api/MockApiClient";
import { AccountInfo, User } from "../../models";
import { updateEmail } from "../../store/userSlice";
import { useDispatch, useSelector } from "react-redux";

const AccountsTab: React.FC = () => {
  const _email = useSelector((state: { user: User }) => state.user.email);
  const dispatch = useDispatch();

  const [email, setEmail] = useState(_email);
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [newPasswordRepeat, setNewPasswordRepeat] = useState("");

  const passwordMeter = new PasswordMeter();
  const passwordStrength = passwordMeter.getResult(newPassword);

  const passwordsMatch = () =>
    newPassword === newPasswordRepeat ? "✅" : "❌";

  const getPasswordScoreOutput = () =>
    `${passwordStrength.percent}% - ${passwordStrength.status.toUpperCase()}`;

  const apiClient = new MockApiClient();

  const handleOnSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!oldPassword) {
      alert("Please enter old password!");
    } else if (!newPassword) {
      alert("Please enter new password!");
    } else if (newPassword !== newPasswordRepeat) {
      alert("Passwords do not match! 😢");
    } else if (passwordStrength.percent < 50) {
      alert(
        "Password too weak! 😢 Please provide a stronger password (i.e. >= 50%)."
      );
    } else {
      const accInfo: AccountInfo = { email, password: newPassword };

      apiClient
        .updateAccountsInfo(accInfo)
        .then(() => {
          dispatch(updateEmail({ email }));
          resetForm();
          alert(
            `Successfully saved Accounts Info 🥳 \n ${JSON.stringify(accInfo)}`
          );
        })
        .catch((err) => {
          alert(`Error in saving Account Info ${err} 😢`);
        });
    }
  };

  const resetForm = () => {
    setOldPassword("");
    setNewPassword("");
    setNewPasswordRepeat("");
  };

  return (
    <>
      <h1>Accounts</h1>
      <form onSubmit={handleOnSubmit}>
        <div className="form-control">
          <label htmlFor="email">Email</label>
          <input
            required
            type="email"
            name="email"
            placeholder="Enter email..."
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <div className="form-control">
          <label htmlFor="oldPassword">Old Password</label>

          <input
            type="password"
            name="oldPassword"
            placeholder="Enter old password..."
            value={oldPassword}
            onChange={(e) => setOldPassword(e.target.value)}
          />
        </div>

        <div className="form-control">
          <label htmlFor="newPassword">New Password</label>
          <input
            type="password"
            name="newPassword"
            placeholder="Enter new password..."
            value={newPassword}
            onChange={(e) => setNewPassword(e.target.value)}
          />
        </div>

        <div className="form-control">
          <label htmlFor="newPasswordRepeat">Repeat New Password</label>
          <input
            type="password"
            name="newPasswordRepeat"
            value={newPasswordRepeat}
            placeholder="Enter new password again..."
            onChange={(e) => setNewPasswordRepeat(e.target.value)}
          />
        </div>

        <div className="password-equality">
          {newPassword && "Passwords Match: " + passwordsMatch()}
        </div>

        <div className="password-score">
          {newPassword && "Password Strength: " + getPasswordScoreOutput()}
        </div>
        <div className="text-center mt">
          <button type="submit">Save</button>
        </div>
      </form>
    </>
  );
};

export default AccountsTab;
