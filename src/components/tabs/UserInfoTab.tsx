import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { MockApiClient } from "../../api/MockApiClient";
import { User, UserInfo } from "../../models";
import { updateUserInfo } from "../../store/userSlice";

const UserInfoTab: React.FC = () => {
  const userInfo = useSelector((state: { user: User }) => state.user.userInfo);
  const dispatch = useDispatch();

  const [firstName, setFirstName] = useState(userInfo.firstName);
  const [lastName, setLastName] = useState(userInfo.lastName);
  const [country, setCountry] = useState(userInfo.country);
  const [street, setStreet] = useState(userInfo.address.street);
  const [houseNum, setHouseNum] = useState(userInfo.address.houseNum);
  const [postCode, setPostCode] = useState(userInfo.address.postCode);

  const counrtryList = ["Germany", "Austria", "Switzerland"];

  const apiClient = new MockApiClient();

  const handleOnSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (
      !firstName ||
      !lastName ||
      !country ||
      !street ||
      !houseNum ||
      !postCode
    ) {
      alert("Incomplete field(s)! 😢");
    } else {
      const _userInfo: UserInfo = {
        firstName,
        lastName,
        country,
        address: {
          street,
          houseNum,
          postCode,
        },
      };

      apiClient
        .updateUnserInfo(_userInfo)
        .then(() => {
          dispatch(updateUserInfo({ userInfo: _userInfo }));
          alert(
            `Successfully saved User Info 🥳 \n ${JSON.stringify(_userInfo)}`
          );
        })
        .catch((err) => {
          alert(`Error in saving User info ${err} 😢`);
        });
    }
  };

  return (
    <>
      <h1>User Information</h1>
      <form onSubmit={handleOnSubmit}>
        <div>
          <h3>Name</h3>

          <div className="form-control">
            <label htmlFor="firstName">First Name</label>
            <input
              required
              type="text"
              name="firstName"
              placeholder="Enter first name..."
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>

          <div className="form-control">
            <label htmlFor="lastName">Last Name</label>
            <input
              required
              type="text"
              name="lastName"
              placeholder="Enter last name..."
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
        </div>

        <div>
          <h3>Address</h3>
          <div className="form-control">
            <label htmlFor="street">Street</label>
            <input
              required
              type="text"
              name="street"
              placeholder="Enter street..."
              value={street}
              onChange={(e) => setStreet(e.target.value)}
            />
          </div>

          <div className="form-control">
            <label htmlFor="houseNum">House Number</label>
            <input
              required
              type="text"
              name="houseNum"
              placeholder="Enter house number..."
              value={houseNum}
              onChange={(e) => setHouseNum(e.target.value)}
            />
          </div>

          <div className="form-control">
            <label htmlFor="postCode">Postal Code</label>
            <input
              required
              type="text"
              name="postCode"
              placeholder="Enter postal code..."
              value={postCode}
              onChange={(e) => setPostCode(e.target.value)}
            />
          </div>
        </div>

        <div className="form-control">
          <label htmlFor="country">Country</label>
          <select
            value={country}
            name="country"
            onChange={(e) => setCountry(e.target.value)}
          >
            {counrtryList.map((c, i) => (
              <option key={i} value={c}>
                {c}
              </option>
            ))}
          </select>
        </div>
        <div className="text-center mt">
          <button type="submit">Save</button>
        </div>
      </form>
    </>
  );
};

export default UserInfoTab;
