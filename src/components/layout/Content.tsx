import React from "react";
import SettingsPage from "../pages/SettingsPage";

const Content: React.FC = () => {
  return (
    <div className="content">
      {/* NOTE: Content can have react router to route to different pages, but that is excluded for now for simplicity */}
      <SettingsPage />
    </div>
  );
};

export default Content;
