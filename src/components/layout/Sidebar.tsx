import React, { useState } from "react";

const Sidebar: React.FC = () => {
  const [activeMenuIdx, setActiveMenuIdx] = useState(0);
  const menuItems = ["Home", "News", "Contact", "About"];

  return (
    <div className="sidebar">
      {menuItems.map((mItem, i) => (
        <a
          onClick={() => setActiveMenuIdx(i)}
          className={activeMenuIdx === i ? "active" : ""}
          key={i}
          href={`!#${mItem}`}
        >
          {mItem}
        </a>
      ))}
    </div>
  );
};

export default Sidebar;
