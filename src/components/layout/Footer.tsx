import React from "react";

const Footer: React.FC = () => {
  return (
    <div className="footer">
      <h4>
        Made with ♡ by <a href="https://github.com/AluBhorta">AluBhorta</a>.
      </h4>
    </div>
  );
};

export default Footer;
