import React from "react";
import { useDispatch } from "react-redux";
import { logout } from "../../store/userSlice";

const Header: React.FC = () => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logout());
  };

  return (
    <>
      <div className="header">
        <a href="!#" className="logo">
          Settings Demo
        </a>

        <div className="header-right">
          <button onClick={handleLogout}>Logout</button>
        </div>
      </div>
    </>
  );
};

export default Header;
